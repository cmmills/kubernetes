# Mongo Database and Mongo Express 

Deploy secrets and configmap first as the deployments require these to be present within the cluster and they reference items from both files.

- ```kubectl create -f <secrets file>```
- ```kubect create -f <config map file>```
